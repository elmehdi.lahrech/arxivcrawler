"""
This script provides a Flask web application for interacting with an arXiv metadata database.

The application provides the following endpoints:

- `/articles/` (GET): Fetches a list of articles from the database and returns a formatted HTML response. -
`/articles/<int:page>` (GET): Fetches a list of articles from the database for a specific page and returns a
formatted HTML response. - `/articles/<path:article_id>` (GET): Retrieves the metadata of an article with the
provided ID. - `/text/<path:article_id>.txt` (GET): Retrieves the abstract of an article based on its ID. -
`/articles` (POST): Uploads an article to the database.

The MongoDB client is configured to connect to a MongoDB instance running on localhost at the port specified by the
`MONGODB_PORT` environment variable, or 27017 if the variable is not set. The client interacts with the `arxiv`
database and the `metadata` collection within that database.

"""
import re
import math
import os
from flask import Flask, Response, url_for, request
from pymongo import MongoClient
from crawler import crawl_db


def format_entry(entry):
    """
    Formats the given entry into a string representation.

    """
    if entry:
        formatted_entry = ""
        for key, value in entry.items():
            if key != "_id":  # Exclude the '_id' field
                if key == "authors":
                    key = key.capitalize()
                    if isinstance(value, dict):
                        value = ", ".join([v for v in value.values()])
                elif key == "msc-class":
                    key = "MSC classes"
                    value = ", ".join(value.split())
                elif key == "doi":
                    key = "Related DOI"
                    value = (
                        f"<a href='https://doi.org/{value}'>https://doi.org/{value}</a>"
                    )
                elif key == "categories":
                    key = "Subjects"
                    value = ", ".join(value.split())
                elif key == "journal-ref":
                    key = "Journal reference"
                elif key == "license":
                    key = key.capitalize()
                    if re.match(
                        r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+",
                        value,
                    ):
                        value = f"<a href='{value}'>{value}</a>"
                elif key == "created":
                    key = "Created on"
                elif key == "updated":
                    key = "Updated on"
                elif key == "id":
                    key = key.upper()
                else:
                    key = key.capitalize()
                formatted_entry += f"<b>{key}:</b> {value}<br>"
        arxiv_link = f"<b>arXiv Link:</b> <a href='https://arxiv.org/abs/{entry['id']}'>https://arxiv.org/abs/{entry['id']}</a><br>"
        pdf_link = f"<b>PDF Link:</b> <a href='https://arxiv.org/pdf/{entry['id']}.pdf'>https://arxiv.org/pdf/{entry['id']}.pdf</a><br>"
        formatted_entry += arxiv_link + pdf_link
        return formatted_entry


app = Flask(__name__)

mongodb_port = int(os.environ.get("MONGODB_PORT", 27017))
client = MongoClient("localhost", mongodb_port)
db = client["arxiv"]
collection = db["metadata"]


@app.route("/", methods=["GET"])
def list_routes():
    """
    Lists all available routes in the application and provides a brief description of each.
    """
    routes = {
        "/articles/": "This route provides a list of all articles, including their metadata. You can also add a page "
                      "number to the end of this URL to view articles by page (e.g., '/articles/1').",
        "/articles/ID": "This route allows you to retrieve metadata for a specific article. Replace 'ID' with the ID "
                        "of the article you're interested in.",
        "/text/ID.txt": "This route allows you to retrieve the abstract of a specific article in text format. Replace "
                        "'ID' with the ID of the article you're interested in.",
        "/articles": "This route allows you to upload a new article to the database. Note: this is a POST route, "
                     "so you'll need to send data with your request."
    }

    links = "<h1>WELCOME!</h1><p>Here are the available routes:</p>"
    for route, description in routes.items():
        links += f'<p><a href="{route}">{route}</a>: {description}</p>'

    return Response(links, mimetype="text/html")


@app.route("/articles/", defaults={"page": 1}, methods=["GET"])
@app.route("/articles/<int:page>", methods=["GET"])
def get_articles(page):
    """
    Fetches a list of articles from the database and returns a formatted HTML response.

    """
    try:
        articles_per_page = 10

        skip = (page - 1) * articles_per_page

        entries = collection.find().skip(skip).limit(articles_per_page)
        total_entries = collection.count_documents({})

        if skip >= total_entries:
            return "No more articles available", 404

        formatted_entries = ""
        for entry in entries:
            if entry:
                formatted_entry = format_entry(entry)
                formatted_entries += formatted_entry + "<hr>"

        last_page_number = math.ceil(total_entries / articles_per_page)
        next_page_url = (
            url_for("get_articles", page=page + 1)
            if (page * articles_per_page) < total_entries
            else None
        )
        prev_page_url = url_for("get_articles", page=page - 1) if page > 1 else None

        if page != 1:
            first_page_url = url_for("get_articles", page=1)
            formatted_entries += f'<a href="{first_page_url}">First</a> | '
        if prev_page_url:
            formatted_entries += f'<a href="{prev_page_url}">Previous</a> | '
        if next_page_url:
            formatted_entries += f'<a href="{next_page_url}">Next</a> | '
        if page != last_page_number:
            last_page_url = url_for("get_articles", page=last_page_number)
            formatted_entries += f'<a href="{last_page_url}">Last</a>'

        return Response(formatted_entries, mimetype="text/html")
    except Exception as e:
        return f"Error fetching data: {str(e)}", 500


@app.route("/articles/<path:article_id>", methods=["GET"])
def get_article_metadata(article_id):
    """
    Retrieves the metadata of an article with the provided ID.

    """
    try:
        entry = collection.find_one({"id": article_id})
        if entry:
            formatted_entry = format_entry(entry)
            return Response(formatted_entry, mimetype="text/html")
        else:
            return "No record found with the provided ID", 404
    except Exception as e:
        return f"Error fetching data: {str(e)}", 500


@app.route("/text/<path:article_id>.txt", methods=["GET"])
def get_article_abstract(article_id):
    """
    Retrieves the abstract of an article based on its ID.

    """
    try:
        print(article_id)
        entry = collection.find_one({"id": article_id})
        if entry:
            return Response(entry["abstract"], mimetype="text/plain")
        else:
            return "No record found with the provided ID", 404
    except Exception as e:
        return f"Error fetching data: {str(e)}", 500


@app.route('/articles', methods=['POST'])
def upload_article():
    """
    Uploads an article to the database.

    """
    required_keys = [
        "id",
        "created",
        "authors",
        "title",
        "categories",
        "comments",
        "journal-ref",
        "doi",
        "license",
        "abstract"
    ]

    try:
        data = request.get_json()
        if not data:
            return "No data provided", 400

        if not all(key in data for key in required_keys):
            return "Data does not contain all required keys", 400

        existing_article = collection.find_one({"id": data.get("id")})
        if existing_article:
            return "Article already exists in the database", 409

        result = collection.insert_one(data)
        return str(result.inserted_id)
    except Exception as e:
        return f"An error occurred while uploading the article: {str(e)}", 500


if __name__ == "__main__":
    timestamp = "from=2024-01-30&"
    # Uncomment the following line to retrieve all 588,176 records in the computer science set.
    # timestamp=""
    if os.path.exists("data/last_crawl.txt"):
        with open("data/last_crawl.txt", "r", encoding="utf-8") as f:
            last_crawl = f.read()
        timestamp = f"from={last_crawl}&"
        crawl_db(timestamp)
    else:
        crawl_db(timestamp)
    app.run()

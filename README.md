# arxivcrawler

A Flask web application for interacting with an arXiv metadata database.

## Description

This web application allows users to fetch, view, and upload article metadata from the arXiv database. The backend is powered by a MongoDB collection where the metadata is stored. The application provides various endpoints for listing articles, viewing details, and uploading new articles.

## Getting Started

### Prerequisites

* Python 3.x
* MongoDB
* Python libraries: `Flask`, `pymongo`, `requests`

### Installation

1. Clone the repository:

    ```bash
    git clone https://gitlab-student.centralesupelec.fr/elmehdi.lahrech/arxivcrawler.git
    cd arxivcrawler
    ```

2. Install the required Python libraries:

    ```bash
    pip install -r requirements.txt
    ```

### Usage

1. Ensure that a MongoDB instance is running on localhost at the specified port (default is 27017).

2. Run the Flask application:

    ```bash
    python app.py
    ```

3. Open your web browser and navigate to http://localhost:5000/ to access the available endpoints.

### Endpoints

* `/`: Lists all available routes in the application.
* `/articles/`: Fetches a list of articles from the database and returns a formatted HTML response.
* `/articles/<int:page>`: Fetches a list of articles from the database for a specific page and returns a formatted HTML response.
* `/articles/<path:article_id>`: Retrieves the metadata of an article with the provided ID.
* `/text/<path:article_id>.txt`: Retrieves the abstract of an article based on its ID.
* `/articles` (POST): Uploads an article to the database.

### Configuration

The MongoDB client is configured to connect to a MongoDB instance running on localhost at the port specified by the MONGODB_PORT environment variable, or 27017 if the variable is not set. The script automatically crawls the arXiv database on startup, updating the local database with the latest information.

### Tests

To run the tests for this application, follow these steps:

1. Make sure you have the required Python libraries installed (see Prerequisites section).

2. In the terminal, navigate to the root directory of the project.

3. Run the following command to execute the tests:

    ```bash
    python -m unittest
    ```

### HTML Snippets

The `html` folder contains snippets from the application.

### Author

Elmehdi LAHRECH

### Version History

- **0.1 (2024-01-31):** Initial Release

### Acknowledgments

* Flask - Web framework for Python
* pymongo - MongoDB driver for Python
* requests - HTTP library for Python

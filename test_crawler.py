import unittest
import xml.etree.ElementTree as ET
from crawler import xml_to_dict, strip_namespace_from_keys


class TestArxivCrawler(unittest.TestCase):
    """
    Unit tests for the ArxivCrawler class.

    """

    def test_xml_to_dict(self):
        """
        Test the xml_to_dict function.

        This test case verifies that the xml_to_dict function correctly converts an XML element to a dictionary.

        """
        xml_data = """<ns0:arXiv xmlns:ns0="http://arxiv.org/OAI/arXiv/" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://arxiv.org/OAI/arXiv/ 
        http://arxiv.org/OAI/arXiv.xsd"> <ns0:id>cs/0112017</ns0:id> <ns0:authors> <ns0:author> 
        <ns0:keyname>Dushay</ns0:keyname> <ns0:forenames>Naomi</ns0:forenames> </ns0:author> </ns0:authors> 
        </ns0:arXiv>"""

        element = ET.fromstring(xml_data)

        result = xml_to_dict(element)

        expected_result = {
            '{http://arxiv.org/OAI/arXiv/}id': 'cs/0112017',
            '{http://arxiv.org/OAI/arXiv/}authors': {
                '{http://arxiv.org/OAI/arXiv/}author': {
                    '{http://arxiv.org/OAI/arXiv/}keyname': 'Dushay',
                    '{http://arxiv.org/OAI/arXiv/}forenames': 'Naomi'
                }
            }
        }
        self.assertEqual(result, expected_result)

    def test_strip_namespace_from_keys(self):
        """
        Test case for the strip_namespace_from_keys function.

        This test verifies that the strip_namespace_from_keys function correctly removes the namespace from the keys
        in the given data dictionary.

        """

        data = {
            '{http://arxiv.org/OAI/arXiv/}keyname': 'Sagawa',
            '{http://arxiv.org/OAI/arXiv/}forenames': 'Shogo'
        }

        result = strip_namespace_from_keys(data)

        expected_result = {
            'keyname': 'Sagawa',
            'forenames': 'Shogo'
        }
        self.assertEqual(result, expected_result)


if __name__ == '__main__':
    unittest.main()

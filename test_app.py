import unittest
from app import app, format_entry


class TestApp(unittest.TestCase):
    """
    Unit tests for app.py.

    """

    def setUp(self):
        """
        Set up the test environment before running each test case.
        - Configures the app for testing.
        - Creates a test client for making requests.
        - Connects to MongoDB and creates a test database.
        - Adds a test article to the database.

        """
        app.config["TESTING"] = True
        self.client = app.test_client()

    def test_format_entry(self):
        """
        Test the format_entry function.

        This test case checks if the format_entry function correctly formats the given entry dictionary into the
        expected result string.

        """
        entry = {
            "id": "1210.8326",
            "created": "2012-10-31",
            "authors": {
                "author_1": "Mikhail Ivanov",
                "author_2": "Fredrik Brännström",
                "author_3": "Alex Alvarado",
                "author_4": "Erik Agrell",
            },
            "title": "General BER Expression for One-Dimensional Constellations",
            "categories": "cs.IT math.IT",
            "comments": "To appear in the Proceedings of the IEEE Global Communications Conference (GLOBECOM) 2012. "
            "Remark 3 modified",
            "journal-ref": "Proc. Global Communications Conference (GlobeCom), Anaheim, CA, Dec. 2012",
            "doi": "10.1109/GLOCOM.2012.6503435",
            "license": "http://arxiv.org/licenses/nonexclusive-distrib/1.0/",
            "abstract": "A novel general ready-to-use bit-error rate (BER) expression for one-dimensional "
            "constellations is developed. The BER analysis is performed for bit patterns that form a "
            "labeling. The number of patterns for equally spaced M-PAM constellations with different BER "
            "is analyzed.",
        }

        expected_result = (
            "<b>ID:</b> 1210.8326<br><b>Created on:</b> 2012-10-31<br><b>Authors:</b> Mikhail Ivanov, "
            "Fredrik Brännström, Alex Alvarado, Erik Agrell<br><b>Title:</b> General BER Expression "
            "for One-Dimensional Constellations<br><b>Subjects:</b> cs.IT, math.IT<br><b>Comments:</b> "
            "To appear in the Proceedings of the IEEE Global Communications Conference (GLOBECOM) "
            "2012. Remark 3 modified<br><b>Journal reference:</b> Proc. Global Communications "
            "Conference (GlobeCom), Anaheim, CA, Dec. 2012<br><b>Related DOI:</b> <a "
            "href='https://doi.org/10.1109/GLOCOM.2012.6503435'>https://doi.org/10.1109/GLOCOM.2012"
            ".6503435</a><br><b>License:</b> <a "
            "href='http://arxiv.org/licenses/nonexclusive-distrib/1.0/'>http://arxiv.org/licenses"
            "/nonexclusive-distrib/1.0/</a><br><b>Abstract:</b> A novel general ready-to-use bit-error "
            "rate (BER) expression for one-dimensional constellations is developed. The BER analysis "
            "is performed for bit patterns that form a labeling. The number of patterns for equally "
            "spaced M-PAM constellations with different BER is analyzed.<br><b>arXiv Link:</b> <a "
            "href='https://arxiv.org/abs/1210.8326'>https://arxiv.org/abs/1210.8326</a><br><b>PDF "
            "Link:</b> <a href='https://arxiv.org/pdf/1210.8326.pdf'>https://arxiv.org/pdf/1210.8326"
            ".pdf</a><br>"
        )

        result = format_entry(entry)
        self.assertEqual(result, expected_result)

    def test_list_routes(self):
        """
        Test case to check if the home page are accessible.

        This method sends a GET request to the root URL ("/") and asserts that the response status code is 200.

        """
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()

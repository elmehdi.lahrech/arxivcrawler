"""
This module contains functions for crawling the arXiv database and storing the metadata in a MongoDB collection.

The MongoDB client is configured to connect to a MongoDB instance running on localhost at the port specified by the
`MONGODB_PORT` environment variable, or 27017 if the variable is not set. The client interacts with the `arxiv`
database and the `metadata` collection within that database.

"""

import datetime
import os
import time
import xml.etree.ElementTree as ET
import requests
from pymongo import MongoClient


def xml_to_dict(element):
    """
    Convert an XML element to a dictionary.

    """
    result = {}
    for child in element:
        if child:
            child_data = xml_to_dict(child)
            if child.tag in result:
                if type(result[child.tag]) is list:
                    result[child.tag].append(child_data)
                else:
                    result[child.tag] = [result[child.tag], child_data]
            else:
                result[child.tag] = child_data
        else:
            result[child.tag] = child.text
    return result


def strip_namespace_from_keys(data):
    """
    Strip the namespace from the keys in a dictionary.

    """
    if isinstance(data, dict):
        return {
            key.split("}")[1] if "}" in key else key: strip_namespace_from_keys(value)
            for key, value in data.items()
        }
    elif isinstance(data, list):
        return [strip_namespace_from_keys(item) for item in data]
    else:
        return data


def crawl_db(timestamp):
    """
    Crawl the arXiv database and store the metadata in a MongoDB collection.

    """
    resumptionToken = None
    added_count = 0
    updated_count = 0

    while True:
        try:
            if resumptionToken:
                url = f"https://export.arxiv.org/oai2?verb=ListRecords&resumptionToken={resumptionToken}"
            else:
                url = f"https://export.arxiv.org/oai2?verb=ListRecords&{timestamp}set=cs&metadataPrefix=arXiv"

            # print("Fetching", url)
            response = requests.get(url)
            response.raise_for_status()
            # print(response.status_code)
        except requests.exceptions.HTTPError as err:
            print(f"HTTP error occurred: {err}")
            if response.status_code == 503:
                print("Sleeping for 5 seconds before retrying...")
                time.sleep(5)
                continue
            else:
                raise
        except Exception as e:
            print(f"An error occurred: {e}")
            raise

        xml_data = response.text
        root = ET.fromstring(xml_data)

        records = root.findall(".//{http://www.openarchives.org/OAI/2.0/}record")

        metadata_list = []
        for record in records:
            arxiv_elements = record.findall(".//{http://arxiv.org/OAI/arXiv/}arXiv")
            if arxiv_elements:
                metadata_dict = xml_to_dict(arxiv_elements[0])

                for key, value in metadata_dict.items():
                    if isinstance(value, str):
                        metadata_dict[key] = value.replace("\n", " ")

                metadata_dict = strip_namespace_from_keys(metadata_dict)

                if "authors" in metadata_dict:
                    authors = metadata_dict["authors"]["author"]
                    authors_dict = {}
                    if isinstance(authors, list):
                        for i, author in enumerate(authors):
                            if isinstance(author, dict):
                                forenames = author.get("forenames", "")
                                keyname = author.get("keyname", "")
                                full_name = f"{forenames} {keyname}"
                                authors_dict[f"author_{i + 1}"] = full_name
                    elif isinstance(authors, dict):
                        forenames = authors.get("forenames", "")
                        keyname = authors.get("keyname", "")
                        full_name = f"{forenames} {keyname}"
                        authors_dict["author_1"] = full_name
                    metadata_dict["authors"] = authors_dict

                if "abstract" in metadata_dict:
                    metadata_dict["abstract"] = metadata_dict["abstract"].strip()

                existing_record = collection.find_one({"id": metadata_dict["id"]})

            if existing_record:
                # print("Record already exists:", metadata_dict["id"])
                existing_record.pop("_id", None)
                if existing_record != metadata_dict:
                    # print("Updating record:", metadata_dict["id"])
                    collection.find_one_and_update(
                        {"id": metadata_dict["id"]}, {"$set": metadata_dict}
                    )
                    updated_count += 1
            else:
                metadata_list.append(metadata_dict)

        if metadata_list:
            collection.insert_many(metadata_list)
            added_count += len(metadata_list)

        resumptionTokenElement = root.find(
            ".//{http://www.openarchives.org/OAI/2.0/}resumptionToken"
        )
        if (
            resumptionTokenElement is not None
            and resumptionTokenElement.text is not None
            and resumptionTokenElement.text.strip() != ""
        ):
            resumptionToken = resumptionTokenElement.text
            time.sleep(3)
        else:
            break

    total_records = collection.count_documents({})
    print("Finished crawling the arXiv database.")
    print(f"Added {added_count} new records.")
    print(f"Updated {updated_count} existing records.")
    print(f"Total records in the database: {total_records}.")
    with open("data/last_crawl.txt", "w", encoding="utf-8") as f:
        f.write(datetime.datetime.now().isoformat())


mongodb_port = int(os.environ.get("MONGODB_PORT", 27017))
client = MongoClient("localhost", mongodb_port)
db = client["arxiv"]
collection = db["metadata"]

if __name__ == "__main__":
    timestamp = "from=2024-01-30&"
    # Uncomment the following line to retrieve all 588,176 records in the computer science set.
    # timestamp=""
    if os.path.exists("data/last_crawl.txt"):
        with open("data/last_crawl.txt", "r", encoding="utf-8") as f:
            last_crawl = f.read()
        timestamp = f"from={last_crawl}&"
        crawl_db(timestamp)
    else:
        crawl_db(timestamp)
